<?php

namespace Workshop\Application;


class Application
{

  /**
   * @var Application $app
   */
  private $app = null;


  /**
   * Initializes framework
   * @param string $configPath
   */
  public static function init(string $configPath)
  {

  }


  /**
   * Runs framework
   */
  public function run()
  {

  }


}